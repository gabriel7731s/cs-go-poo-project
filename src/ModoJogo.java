public class ModoJogo {

	private String nome;
	private double duracaoPartida;
	private int qtdeJogadores;
	
	
	public String getNome () {
		return nome;
	}
	
	public double getDuracaoPartida () {
		return duracaoPartida;
	}
	
	public int getQtdeJogadores () {
		return qtdeJogadores;		
	}
	
	
	
	public void setNome(String nome) {
		this.nome = nome;		
	}
	
	public void setDuracaoPartida(double duracaoPartida) {
		this.duracaoPartida = duracaoPartida;		
	}
	
	public void setQtdeJogadores(int qtdeJogadores) {
		this.qtdeJogadores = qtdeJogadores;		
	}
	
}
