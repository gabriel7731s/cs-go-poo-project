import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Principal {

	public static void main(String [] args) {
		Principal principal = new Principal();
		principal.menu("geral");
	}	
	
	private void menu(String escolherOpcao) {
		if (escolherOpcao == "geral") {
			System.out.println("1 - Novo | 2 - Listar");		
		}
		switch (opcaoMenu(escolherOpcao)) {
		case 1:
			System.out.println("1 - Novo jogador | 2 - Novo modo de jogo | 3 - Novo equipamento | 4 - Voltar");
			novo(leitura.nextInt());			
		case 2:
			System.out.println("1 - Listar jogadores | 2 - Listar modos de jogo | 3 - Listar equipamentos | 4 - Voltar");
			listar(leitura.nextInt());
		default:
			break;
		}
	}
	
	private int opcaoMenu(String escolherOpcao) {
		if (escolherOpcao == "geral") {
			return leitura.nextInt();		
		}else if (escolherOpcao == "novo"){
			return 1;
		}else {
			return 2;
		}
	}
	
	private void novo(int opcao) {
		switch (opcao) {
		case 1:
			novoJogador();
			break;
		case 2:
			novoModoJogo();
			break;
		case 3:
			System.out.println("1 - Nova arma de fogo | 2 - Nova arma branca | 3 - Novo colete | 4 - Voltar");
			if (leitura.nextInt() == 1) {
				novaArmaFogo();
			}else if (leitura.nextInt() == 2) {
				
			}else if (leitura.nextInt() == 3) {
				
			}else if (leitura.nextInt() == 4) {
				menu("novo");
			}
			break;
		case 4:
			menu("geral");
			break;
		default:
			break;			
		}
	}
	
	private void listar(int opcao) {
		switch (opcao) {
		case 1:
			listaJogadores();
			break;
		case 2:
			listaModosJogo();
			break;
		case 3:
			System.out.println("1 - Listar armas de fogo | 2 - Listar armas brancas | 3 - Listar coletes | 4 - Voltar");
			if (leitura.nextInt() == 1) {
				listaArmasFogo();
			}else if (leitura.nextInt() == 2) {
								
			}else if (leitura.nextInt() == 3) {
				
			}else if (leitura.nextInt() == 4) {
				menu("listar");
			}
			break;
		case 4:
			menu("geral");
			break;
		default:
			break;			
		}
	}
	
	Scanner leitura = new Scanner(System.in);
	
	List<Jogador> jogadores = new ArrayList<Jogador>();
	
	List<ModoJogo> modosJogo = new ArrayList<ModoJogo>();
	
	List<ArmaFogo> armasFogo = new ArrayList<ArmaFogo>();
	
	List<ArmasBrancas> armasBrancas = new ArrayList<ArmasBrancas>();
	
	private void novoJogador() {
		int qtdeJogadores = 0;
		while (qtdeJogadores <= 0) {
			System.out.println("Deseja inserir quantos jogadores?");
			qtdeJogadores = leitura.nextInt();		
		}		
		for (int i = 1; i <= qtdeJogadores; i++) {
			Jogador jogador = new Jogador();
			
			System.out.println("Nome do jogador: ");
			jogador.setNomeUsuario(leitura.next());
			
			System.out.println("N�vel do jogador: ");
			jogador.setNivel(leitura.nextInt());
			
			System.out.println("Patente do jogador: ");
			jogador.setPatente(leitura.next());
			
			jogadores.add(jogador);
		}
	}
	
	private void listaJogadores() {
		for (Jogador jogador : jogadores) {
			System.out.println("Nome: " + jogador.getNomeUsuario());
			System.out.println("N�vel: " + jogador.getNivel());
			System.out.println("Patente: " + jogador.getPatente());
			System.out.println("--- --- ---");	
		}
	}
	
	
	private void novoModoJogo() {
		int qtdeModoJogo = 0;
		while (qtdeModoJogo <= 0) {
			System.out.println("Deseja inserir quantos modos de jogo?");
			qtdeModoJogo = leitura.nextInt();		
		}		
		for (int i = 1; i <= qtdeModoJogo; i++) {
			ModoJogo modoJogo = new ModoJogo();
			
			System.out.println("Nome do modo de jogo: ");
			modoJogo.setNome(leitura.next());
			
			System.out.println("N�vel do jogador: ");
			modoJogo.setDuracaoPartida(leitura.nextDouble());
			
			System.out.println("Patente do jogador: ");
			modoJogo.setQtdeJogadores(leitura.nextInt());
			
			modosJogo.add(modoJogo);
		}
	}

	private void listaModosJogo() {
		for (ModoJogo modoJogo: modosJogo) {		
			System.out.println("Nome: " + modoJogo.getNome());
			System.out.println("Dura��o da partida: " + modoJogo.getDuracaoPartida());
			System.out.println("M�ximo de jogadores: " + modoJogo.getQtdeJogadores());
			System.out.println("--- --- ---");
		}
	}

	
	private void novaArmaFogo() {
		int qtdeArmasFogo = 0;
		while (qtdeArmasFogo <= 0) {
			System.out.println("Deseja inserir quantas armas de fogo?");
			qtdeArmasFogo = leitura.nextInt();		
		}		
		for (int i = 1; i <= qtdeArmasFogo; i++) {
			ArmaFogo armaFogo = new ArmaFogo();
			
			System.out.println("Nome da arma de fogo: ");
			armaFogo.setNome(leitura.next());
			
			System.out.println("Pre�o da arma de fogo: ");
			armaFogo.setPreco(leitura.nextInt());
			
			System.out.println("Peso da arma de fogo: ");
			armaFogo.setPeso(leitura.nextDouble());
			
			System.out.println("Dano da arma de fogo: ");
			armaFogo.setDano(leitura.nextInt());
			
			System.out.println("Alcance da arma de fogo: ");
			armaFogo.setAlcance(leitura.nextDouble());
			
			System.out.println("Capacidade do pente: ");
			armaFogo.setCapacidadePente(leitura.nextInt());
						
			armasFogo.add(armaFogo);
		}
	}
	
	private void listaArmasFogo() {
		for (ArmaFogo armaFogo: armasFogo) {		
			System.out.println("Nome da arma de fogo: " + armaFogo.getNome());
			System.out.println("Pre�o da arma de fogo: " + armaFogo.getPreco());
			System.out.println("Peso da arma de fogo: " + armaFogo.getPeso());
			System.out.println("Dano da arma de fogo: " + armaFogo.getDano());
			System.out.println("Alcance da arma de fogo: " + armaFogo.getAlcance());
			System.out.println("Capacidade do pente da arma de fogo: " + armaFogo.getCapacidadePente());
			System.out.println("--- --- ---");
		}
	}
	
	
	private void novaArmaBranca() {
		int qtdeArmaBranca = 0;
		while (qtdeArmaBranca <= 0) {
			System.out.println("Deseja inserir quantas armas brancas?");
			qtdeArmaBranca = leitura.nextInt();		
		}		
		for (int i = 1; i <= qtdeArmaBranca; i++) {
			ArmaBranca armaBranca = new ArmaBranca();
			
			System.out.println("Nome da arma branca: ");
			armaBranca.setNome(leitura.next());
			
			System.out.println("Pre�o da arma branca: ");
			armaBranca.setPreco(leitura.nextInt());
			
			System.out.println("Peso da arma branca: ");
			armaBranca.setPeso(leitura.nextDouble());
			
			System.out.println("Dano da arma branca: ");
			armaBranca.setDano(leitura.nextInt());
			
			System.out.println("Alcance da arma branca: ");
			armaBranca.setAlcance(leitura.nextDouble());
			
			System.out.println("Capacidade branca: ");
			armaBranca.setCapacidadePente(leitura.nextInt());
						
			.add(armaFogo);
		}
	}
}
