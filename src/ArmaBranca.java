
public class ArmaBranca extends Equipamento{

	private int dano;
	private String tipo;

	
	public int getDano() {
		return dano;
	}
	
	public String getTipo() {
		return tipo;
	}
	
	
	public void setDano(int dano) {
		this.dano = dano;
	}
	
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
		
}
