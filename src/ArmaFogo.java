public class ArmaFogo extends Equipamento{

	private int dano;
	private double alcance;
	private int capacidadePente;
	
	
	public int getDano() {
		return dano;
	}
	
	public double getAlcance() {
		return alcance;
	}
	
	public int getCapacidadePente() {
		return capacidadePente;
	}
	
	
	public void setDano(int dano) {
		this.dano = dano;
	}
	
	public void setAlcance(double alcance) {
		this.alcance = alcance;
	}
	
	public void setCapacidadePente(int capacidadePente) {
		this.capacidadePente = capacidadePente;
	}

}
