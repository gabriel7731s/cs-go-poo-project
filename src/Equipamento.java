public class Equipamento {
	
	private String nome;
	private double peso;
	private int preco;
	
	public String getNome() {
		return nome;
	}
	
	public double getPeso() {
		return peso;
	}
	
	public int getPreco() {
		return preco;
	}
	
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public void setPeso(double peso) {
		this.peso = peso;
	}
	
	public void setPreco(int preco) {
		this.preco = preco;
	}

}
