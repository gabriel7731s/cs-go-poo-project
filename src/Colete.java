public class Colete extends Equipamento{

	private int protecao;
	private String cor;
	
	
	public int getProtecao() {
		return protecao;
	}
	
	public String getCor() {
		return cor;
	}
	
	
	public void setProtecao(int protecao) {
		this.protecao = protecao;
	}
	
	
	public void setCor(String cor) {
		this.cor = cor;
	}
		
}
