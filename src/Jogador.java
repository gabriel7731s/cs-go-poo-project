public class Jogador {
		
	private String nomeUsuario;
	private int nivel;
	private String patente;
	
	
	public String getNomeUsuario() {
		return nomeUsuario;
	}
	
	public int getNivel() {
		return nivel;
	}
	
	public String getPatente() {
		return patente;
	}
	
	
	public void setNomeUsuario(String nomeUsuario) {
		this.nomeUsuario = nomeUsuario;
	}
	
	public void setNivel(int nivel) {
		this.nivel = nivel;
	}
	
	public void setPatente(String patente) {
		this.patente = patente;
	}
	
}
